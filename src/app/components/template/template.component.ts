import { Component, OnInit } from '@angular/core';
import { Datos } from 'src/app/interfaces/interface';

@Component({
  selector: 'app-template',
  templateUrl: './template.component.html',
  styleUrls: ['./template.component.css']
})
export class TemplateComponent implements OnInit {
  persona: Datos = {
    nombre:'',
    direccion:'',
    terminos:false

   };
  constructor() { }

  ngOnInit(): void {
  }

  guardar():void{
    console.log("envio del submit");
    console.log(this.persona.nombre);
    console.log(this.persona.direccion);
    console.log(this.persona.terminos);
    

  }

}
